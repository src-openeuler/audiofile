Name: audiofile
Version: 0.3.6
Release: 29
Summary: Library for reading and writing audio files in many common formats
License: LGPLv2+ and GPLv2+
URL: http://audiofile.68k.org/
Source0: http://audiofile.68k.org/%{name}-%{version}.tar.gz
Patch0: audiofile-CVE-2015-7747.patch
Patch1: audiofile-fix-gcc6-compile-error.patch
Patch2: audiofile-fix-test-compile-err.patch
Patch3: backport-CVE-2017-6828.patch
Patch4: backport-CVE-2017-6829.patch
Patch5: backport-CVE-2017-6831.patch
Patch6: backport-CVE-2017-6838.patch
Patch7: backport-CVE-2017-6839.patch
Patch8:backport-CVE-2022-24599.patch

BuildRequires: gcc-c++ libtool alsa-lib-devel flac-devel chrpath

%description
The Audio File Library is a C-based library for reading and writing audio files in many
common formats.

The Audio File Library provides a uniform API which abstracts away details of file formats
and data formats. The same calls for opening a file, accessing and manipulating audio
metadata (e.g. sample rate, sample format, textual information, MIDI parameters), and
reading and writing sample data will work with any supported audio file format.

%package devel
Summary: Development files for Audio File applications
Requires: %{name}%{?_isa} = %{version}-%{release}
Provides: audiofile-static
Obsoletes: audiofile-static

%description devel
The audiofile-devel package contains libraries, include files, and
other resources you can use to develop Audio File applications.

%package_help

%prep
%autosetup -n %{name}-%{version} -p1

%build
%configure
%make_build

%install
rm -rf $RPM_BUILD_ROOT
%make_install
rm -f $RPM_BUILD_ROOT%{_libdir}/libaudiofile.la
chrpath --delete %{buildroot}%{_bindir}/sfinfo
chrpath --delete %{buildroot}%{_bindir}/sfconvert

#%check
#make check

%pre

%preun

%post

%postun

%ldconfig_scriptlets

%files
%doc ACKNOWLEDGEMENTS AUTHORS NEWS NOTES README TODO COPYING*
%{_bindir}/sfconvert
%{_bindir}/sfinfo
%{_libdir}/lib*.so.1*

%files devel
%doc ChangeLog docs/*.3.txt
%{_libdir}/libaudiofile.so
%{_libdir}/pkgconfig/audiofile.pc
%{_includedir}/*
%{_libdir}/libaudiofile.a

%files help
%{_mandir}/man1/*
%{_mandir}/man3/*

%changelog
* Fri Jan 10 2025 pengjian <pengjian23@mails.ucas.ac.cn> - 0.3.6-29
- enhances error checking and boundary verification in memory allocation 
to fix potential security vulnerabilities during audio file analysis.

* Tue Nov 15 2022 dillon chen <dillon.chen@gmail.com> - 0.3.6-28
- Remove check when flac >= 1.3.4

* Mon Sep 6 2021 Hongxun Ren<renhongxun@huawei.com> - 0.3.6-27
- Type:enhanence
- ID:NA
- SUG:NA
- DESC:remove rpath

* Thu Jul 22 2021 wuchaochao <wuchaochao4@huawei.com> - 0.3.6-26
- Remove BuildRequires gdb

* Fri Feb 19 2021 shixuantong<shixuantong@huawei.com> - 0.3.6-25
- Type:cves
- ID:CVE-2017-6828 CVE-2017-6829 CVE-2017-6831 CVE-2017-6838 CVE-2017-6839
- SUG:NA
- DESC:fix CVE-2017-6828 CVE-2017-6829 CVE-2017-6831 CVE-2017-6838 CVE-2017-6839

* Sat Mar 21 2020 Shouping Wang<wangshouping@huawei.com> - 0.3.6-24
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:fix test compile err

* Thu Jan  9 2020 JeanLeo<liujianliu.liu@huawei.com> - 0.3.6-23
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:update software package

* Fri Sep 27 2019 chengquan<chengquan3@huawei.com> - 0.3.6-22
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:add help package and merge devel package

* Tue Sep 17 2019 shenyangyang<shenyangyang4@huawei.com> - 0.3.6-21
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:and % before isa of devel

* Tue Aug 13 2019 openEuler Buildteam <buildteam@openeuler.org> - 0.3.6-20
- Package init

